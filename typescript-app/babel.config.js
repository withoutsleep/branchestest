module.exports = {
  presets: [
    '@vue/cli-plugin-babel/preset'
  ],
  parserOptions: {
    ecmaFeatures: {
      legacyDecorators: true
    }
  }
  
}
